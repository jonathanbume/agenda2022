﻿using _0._0.DataTransfer.Generic;
using System.ComponentModel.DataAnnotations;

namespace _0._0.DataTransfer.Dto
{
    public class DtoUsuario:DtoGeneric
    {
        public string idUsuario { get; set; }
        [Required(ErrorMessage = "el campo \"idContraseña\"es requerido.")]
        public string idContraseña { get; set; }
        [Required(ErrorMessage = "el campo \"Nombres\"es requerido.")]
        public string Nombres { get; set; }
        [Required(ErrorMessage = "el campo \"Apellidos\"es requerido.")]
        public string Apellidos { get; set; }
        [Required(ErrorMessage = "el campo \"DNI\"es requerido.")]
        public string Dni { get; set; }
        [Required(ErrorMessage = "el campo \"NumTelefonico\"es requerido.")]
        public string NumeroTelefonico { get; set; }
        public string Direccion { get; set; }
        public string EmailUsuario { get; set; }
        [Required(ErrorMessage = "el campo \"Direccion\"es requerido.")]
        public DateTime FechaNacimiento { get; set; }
        [Required(ErrorMessage = "el campo \"CodPostal\"es requerido.")]
        public string CodigoPostal { get; set; }

    }
}
