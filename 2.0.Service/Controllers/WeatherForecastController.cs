using Microsoft.AspNetCore.Mvc;

namespace _2._0.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "saludos";
        }
    }
}