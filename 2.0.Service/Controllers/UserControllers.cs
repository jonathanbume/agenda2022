using _2._0.Service.Generic;
using _2._0.Service.ServiceObject;
using _3._0.Business.Business.Usuario;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
namespace _2._0.Service.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("cors")]
    public class UserController : ControllerGeneric<SoUser, BusinessUser>
    {
        [HttpPost]
        [Route("[action]")]
        public ActionResult<SoUser> Login([FromForm] SoUser so)
        {
            try
            {
                _so.mo = ValidatePartDto(so.dtoUser, new string[] {
                    "Dni"
                });

                if (_so.mo.existsMessage())
                {
                    return _so;
                }

                _so.mo = _business.Insert(so.dtoUser);

                return _so;
            }
            catch (Exception e)
            {
                _so.mo.listMessage.Add(e.Message);
                _so.mo.exception();

                return _so;
            }
        }
        [HttpPost]
        [Route("[action]")]
        public ActionResult<SoUser> Insert([FromForm] SoUser so)
        {
            try
            {
                _so.mo = ValidatePartDto(so.dtoUser, new string[] {
                    "Dni"
                });

                if (_so.mo.existsMessage())
                {
                    return _so;
                }

                _so.mo = _business.Insert(so.dtoUser);

                return _so;
            }
            catch (Exception e)
            {
                _so.mo.listMessage.Add(e.Message);
                _so.mo.exception();

                return _so;
            }
        }
        [HttpGet]
        [Route("[action]")]
        public ActionResult<SoUser> GetAll()
        {
            try
            {
                (_so.mo, _so.listDtoService) = _business.GetAll();

                return _so;
            }
            catch (Exception e)
            {
                _so.mo.listMessage.Add(e.Message);
                _so.mo.exception();

                return _so;
            }
        }

        [HttpDelete]
        [Route("[action]")]
        public ActionResult<SoUser> Delete(string idService)
        {
            try
            {
                _so.mo = _business.Delete(idService);

                return _so;
            }
            catch (Exception e)
            {
                _so.mo.listMessage.Add(e.Message);
                _so.mo.exception();

                return _so;
            }
        }

        [HttpPost]
        [Route("[action]")]
        public ActionResult<SoUser> Edit([FromForm] SoUser so)
        {
            try
            {
                _so.mo = ValidatePartDto(so.dtoUser, new string[] {
                    "Dni"
                
                });

                if (_so.mo.existsMessage())
                {
                    return _so;
                }

                _so.mo = _business.Edit(so.dtoUser);

                return _so;
            }
            catch (Exception e)
            {
                _so.mo.listMessage.Add(e.Message);
                _so.mo.exception();

                return _so;
            }
        }
    }
}



