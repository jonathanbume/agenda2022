﻿using _0._0.DataTransfer.Dto;
using _2._0.Service.Generic;

namespace _2._0.Service.ServiceObject
{
    public class SoUser : SoGeneric
    {
        public DtoUsuario dtoUser { get; set; }

        public List<DtoUsuario> listDtoService { get; set; }
    }
}