﻿

using _5._0.DataAccess.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _5._0.DataAccess.Entity
{
    [Table("tactividades")]
    public class Actividad :EntityGeneric
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string idActividad { get; set; }
        public string idUsuario { get; set; }
        public string Titulo { get; set; }
        public DateTime FechaProgramada { get; set; }
        public DateTime HoraProgramada { get; set; }

        [ForeignKey(nameof(idUsuario))]
        public Usuario parentUsuario { get; set; }
    }
}
    