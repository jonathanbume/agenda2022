﻿using _5._0.DataAccess.Generic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._0.DataAccess.Entity
{
    [Table("tlogin")]
    public  class LoginUser
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
            public string idUser { get; set; }
            public string EmailUsuario { get; set; }
            public string Password { get; set; }

    }
}
